# config-diff

![Build Status](https://gitlab.com/luca_beetz/config-diff/badges/master/build.svg)

## Goal

This tool downloads the config files of multiple switches via SFTP and detects changes to them using `git status`.
Any changes are committed. The commit message contains an overview of which switches have been updated.

## Build and install

The included Makefile can be used to build and install the tool. For this `cargo` is required. 
Information on installing it can be found here: https://www.rust-lang.org/tools/install. 
Please also make sure to have openssl installed, which should be installed by default on most *nix systems.

After installing cargo run the command `make install` to build and install the package.

## Setup

A `config.toml` file, containing information about each switch, is required for the tool to work.
You can then run the tool using the command `config-diff <CONFIG-PATH>`. The absolute path to the config file must
be provided. The directory in which the tool is run must be a git repo: `git init`

## Running into problems

Please follow the instructions on the screen. Feel free to create an issue for any problem you encounter.
Suggestions on how the tool can be improved are very welcome.

### Example config file

```toml
[[switches]]
username = "user1"
password = "secure"
hostname = "test-switch"
ip_address = "127.0.0.1:22"
remote_files = ["cfg/config", "cfg/secconfig"]

[[switches]]
username = "user2"
password = "sec"
hostname = "test-switch"
ip_address = "192.168.23.4:22"
remote_files = ["cfg/config", "cfg/secconfig"]
```
