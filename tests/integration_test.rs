extern crate config_diff;

use config_diff::*;

#[test]
fn test_life() {
    assert_eq!(2 + 2, 4);
}

#[test]
fn test_config_parsing() {
    let config = Config::load_config("./tests/test_config.toml").unwrap();

    assert_eq!(config.switches[0].username, "test_name");
    assert_eq!(config.switches[0].password, "passw0rd");
}