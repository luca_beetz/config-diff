use std::fs::File;
use std::io::prelude::*;
use colored::*;
use clap::{App, Arg};

mod lib;

fn main() {
    let matches = App::new("config-diff")
                    .version("0.1.0")
                    .author("Luca Beetz <luca-beetz@tutanota.com>")
                    .about("Checks switch config files for updates")
                    .arg(Arg::with_name("config")
                        .short("c")
                        .long("config")
                        .value_name("FILE")
                        .help("Sets the config file containing info about all switches")
                        .required(true))
                    .get_matches();

    let config_file_path = matches.value_of("config").unwrap();

    // Load and parse config file
    let config = match lib::Config::load_config(config_file_path) {
        Ok(config) => {
            println!("{} Config has been loaded", "Success:".green());
            config
        },
        Err(err) => {
            eprintln!("{} There has been an error while loading the config file: {}", "Error:".red(), err);
            std::process::exit(-2);
        }
    };

    println!();
    println!("{} {}", "Info: ".yellow(), "----- Start downloading configs -----".cyan());
    println!();

    let mut updated_switches: Vec<&lib::SwitchConfig> = Vec::new();

    // Loop through all switches
    for switch_config in &config.switches {
        println!("{} Connecting to switch: {} | IP: {}", "Info:".yellow(), switch_config.hostname, switch_config.ip_address);

        // Download remote files and write to local file
        let remote_config_buffers = match lib::download_config_files(switch_config) {
            Ok(file_buffers) => {
                println!("{} The remote config files have been downloaded", "Success:".green());
                file_buffers
            },
            Err(err) => {
                eprintln!("{} There has been an error while downloading the remote files: {}", "Error:".red(), err);
                continue;
            }
        };

        // Create dir for switch if it doesn't exist yet
        std::fs::create_dir_all(switch_config.hostname.as_str()).expect("Unable to create dir for config");

        let mut switch_config_has_changed = false;

        for (i, remote_config_buffer) in remote_config_buffers.iter().enumerate() {
            let remote_file_name = match switch_config.remote_files.get(i) {
                Some(file_name) => file_name.split('/').last().expect("Unable to parse file name"),
                None => {
                    eprintln!("{} There has been an error while reading the config file names", "Error:".red());
                    continue;
                }
            };

            let local_file_name = format!("config_{}_{}.txt", switch_config.hostname, remote_file_name);
            let local_file_path = format!("{}/{}", switch_config.hostname, local_file_name);

            let mut local_file = match File::create(&local_file_path) {
                Ok(file) => file,
                Err(err) => {
                    eprintln!("{} There has been an error while trying to create/open a local config file: {}", "Error:".red(), err);
                    continue;
                }
            };

            match local_file.write_all(remote_config_buffer.as_bytes()) {
                Ok(()) => {},
                Err(err) => {
                    eprintln!("{} There has been an error while trying to write to a local config file: {}", "Error:".red(), err);
                    continue;
                }
            };

            // Check whether there have been any changes to the files
            switch_config_has_changed = match lib::has_file_changed(switch_config.hostname.as_str()) {
                Ok(config_changed) => config_changed,
                Err(err) => {
                    eprintln!("{} There has been an error while checking whether the remote files have been changed: {}", "Error:".red(), err);
                    continue;
                },
            };
        }

        if switch_config_has_changed {
            updated_switches.push(&switch_config);
        }

    }

    println!();
    println!("{} {}", "Info: ".yellow(), "----- Downloaded all configs -----".cyan());
    println!();

    // Commit possible changes
    if !updated_switches.is_empty() {
        println!("{} Remote config files have changed", "Info:".yellow());

        let mut commit_message = format!("Number of updated switches: {}\n\n", updated_switches.len());

        commit_message.push_str("----- Updated switches -----\n");
        for updated_switch in &updated_switches {
            commit_message.push_str(format!("Hostname: {} | IP: {}\n", updated_switch.hostname, updated_switch.ip_address).as_str());
        }

        match lib::add_and_commit_changes(&commit_message) {
            Ok(_) => {
                println!("{} Changes have been committed", "Success:".green());
                std::process::exit(1);
            },
            Err(err) => {
                eprintln!("{} There has been an error while running git add/commit: {}", "Error:".red(), err);
                std::process::exit(-1);
            }
        }
    } else {
        println!("{} Remote files have not changed", "Info:".yellow());
    }
}

