use std::fs::File;
use std::io::prelude::*;
use std::net::TcpStream;
use std::path::Path;
use std::str;
use std::io;
use std::process::Command;
use ssh2::Session;
use toml;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub switches: Vec<SwitchConfig>,
}

#[derive(Deserialize)]
pub struct SwitchConfig {
    pub hostname: String,
    pub username: String,
    pub password: String,
    pub ip_address: String,
    pub remote_files: Vec<String>,
}

type Result<T> = std::result::Result<T, failure::Error>;

impl Config {
    pub fn load_config(file_path: &str) -> Result<Self> {
        let mut file = File::open(file_path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let config: Self = toml::from_str(&contents)?;
        Ok(config)
    }
}

/// Downloads the config files of the switch in [switch_config] and returns it
///
/// # Arguments
///
/// * `switch_config` - A struct containing information about the switch
///
pub fn download_config_files(switch_config: &SwitchConfig) -> Result<Vec<String>> {
    let tcp = TcpStream::connect(&switch_config.ip_address)?;
    let mut sess = Session::new().ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Unable to open new session"))?;
    sess.handshake(&tcp)?;

    // Authentication
    sess.userauth_password(switch_config.username.as_str(), switch_config.password.as_str())?;
    assert!(sess.authenticated());

    let sftp_channel = sess.sftp()?;

    let mut file_buffers: Vec<String> = Vec::new();

    for file_path in &switch_config.remote_files {
        let mut buffer: String = String::new();
        let mut file = sftp_channel.open(Path::new(file_path))?;

        file.read_to_string(&mut buffer)?;
        file_buffers.push(buffer);
    }

    Ok(file_buffers)
}

/// Checks whether [file_name] has changed using git status`
///
/// # Arguments
///
/// * `file_name` - The name of the file
///
pub fn has_file_changed(file_name: &str) -> Result<bool> {
    let status_output = Command::new("git")
        .arg("status")
        .arg("--porcelain")
        .output()?;

    let has_changed = str::from_utf8(&status_output.stdout)?.contains(file_name);
    Ok(has_changed)
}

/// Add all changes and commit them with the [commit_message]
///
/// # Arguments
///
/// * `commit_message` - The message of the commit
///
pub fn add_and_commit_changes(commit_message: &str) -> Result<()> {
    // Run git add local_config.txt
    Command::new("git")
        .arg("add")
        .arg("*")
        .spawn()?
        .wait()?;

    // Run git commit
    Command::new("git")
        .arg("commit")
        .arg("-m")
        .arg(commit_message)
        .spawn()?
        .wait()?;

    Ok(())
}
